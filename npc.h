#ifndef NPC_H
#define NPC_H
#include <iostream>
#include <vector>

#include "equipment.h"

namespace npc
{

class dialogue
{
public:
    std::string line;
    bool available;
};

class npc
{
public:
    enum class npc_type {seller, enemy, neutral, gatekeeper};
private:
    std::string name;
    int lvl;
    int id;
    npc_type type;
    int base_dmg;
    equipment::equipment weapon;
    std::vector<equipment::equipment> wares;

    std::vector<dialogue> dialogue;
};
}


#endif // NPC_H

#include <iostream>
#include "equipment.h"

using namespace std;

int main()
{
    vector<equipment::equipment> lvl_1_equipment;
    equipment::get_lvl_1_equipment(lvl_1_equipment);
    lvl_1_equipment[0].print();
    return 0;
}

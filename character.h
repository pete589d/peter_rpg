#ifndef CHARACTER_H
#define CHARACTER_H
#include <iostream>
#include <string.h>

#include "equipment.h"

namespace character
{
    class character
    {
    private:
        std::string name;
        int lvl;
        int xp;
        int id;
        int money;
        int max_weight;
        int equipment_weight;
        std::vector<equipment::equipment> equipment;
    public:
        character(std::string name);

        std::string get_name();
        int get_lvl();
        int get_xp();
        int get_id();
        int get_money();
        int get_max_weight();
        int get_equipment_weight();
        std::vector<equipment::equipment> get_equipment();

        void set_equipment_weight();
        void add_money(int change);
        void buy(equipment::equipment purchase);
        bool affordable(equipment::equipment purchase);
        bool capacity(equipment::equipment purchase);

    };


}

#endif // CHARACTER_H

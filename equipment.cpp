#include "equipment.h"
namespace equipment
{
    equipment::equipment(std::string name, int price, equip_type type, int weight, std::string description, bool available)
    {
        if (name != "")
        this->name = name;

        this->price = price;
        this->type = type;
        this->weight = weight;
        if (description != "")
        this->description = description;

        this->availabe = available;
    }

    void equipment::make_avail()
    {
        this->availabe = true;
    }

    void equipment::make_unavail()
    {
        this->availabe = false;
    }



    std::string equipment::print_type()
    {
        switch(this->type)
        {
        case equip_type::weapon:
            return "weapon";
        case equip_type::ammo:
            return "ammo";
        case equip_type::armor:
            return "armor";
        case equip_type::trinket:
            return "trinket";
        case equip_type::consumable:
            return "consumable";
        }
    }

    void equipment::print()
    {
        std::cout << this->name << ":\n" << print_type() <<"\n" << this->price << "\n" << this->weight << "\n" << this->description << "\n" <<[&](){if(this->availabe)return("Available");else return("Not available");}() << "\n";
    }

    void get_lvl_1_equipment(std::vector<equipment>& equip)
    {
        equip.push_back({"Wooden Sword", 10, equipment::equipment::equip_type::weapon, 500, "Sword made from oak", true});
    }



    //returns private members from now on
    std::string equipment::get_name()
    {
        return this->name;
    }

    int equipment::get_price()
    {
        return this->price;
    }
    equipment::equip_type equipment::get_type()
    {
        return this->type;
    }
    int equipment::get_weight()
    {
        return this->weight;
    }

    std::string equipment::get_desc()
    {
        return this->description;
    }
    bool equipment::get_avail()
    {
        return this->availabe;
    }


}

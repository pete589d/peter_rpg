#include "character.h"
#include <ctime>

namespace character
{

    character::character(std::string name)
    {
       if (name != "")
       this->name = name;

       std::srand(static_cast<unsigned int>(std::time(nullptr)));
       this->id = rand() % (99999 + 1 - 10000) + 10000;
       this->money = 100;
       this->max_weight = 10000;

       set_equipment_weight();

    }

    void character::set_equipment_weight()
    {
        for (equipment::equipment current : this->equipment)
        {
            this->equipment_weight += current.get_weight();
        }
    }

    void character::add_money(int change)
    {
        if(change > 0)
        this->money += change;
    }

    bool character::affordable(equipment::equipment purchase)
    {
        return (purchase.get_price() <= this->money);
    }

    bool character::capacity(equipment::equipment purchase)
    {
        return (purchase.get_weight() <= this->max_weight-this->equipment_weight);
    }

    void character::buy(equipment::equipment purchase)
    {
        if (affordable(purchase) and capacity(purchase))
        {
            this->money -= purchase.get_price();
            this->equipment.push_back(purchase);
        }

    }

    //returns private members from now on
    std::string character::get_name()
    {
        return this->name;
    }
    int character::get_lvl()
    {
        return this->lvl;
    }

    int character::get_xp()
    {
        return this->xp;
    }

    int character::get_id()
    {
        return this->id;
    }
    int character::get_money()
    {
        return this->money;
    }
    int character::get_max_weight()
    {
        return this->max_weight;
    }
    int character::get_equipment_weight()
    {
        return this->equipment_weight;
    }
    std::vector<equipment::equipment> character::get_equipment()
    {
        return this->equipment;
    }

}

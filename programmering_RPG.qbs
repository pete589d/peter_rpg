import qbs

CppApplication {
    consoleApplication: true
    files: [
        "character.cpp",
        "character.h",
        "equipment.cpp",
        "equipment.h",
        "main.cpp",
        "npc.cpp",
        "npc.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
